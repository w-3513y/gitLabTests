using Xunit;

namespace gitLabTests;

public class UnitTest1
{
    [Fact]
    public void IsFalse()
    {
        int value1 = 1;
        int value2 = 2;
        Assert.False(value1 > value2, "2 should be higher than 1");
    }

    [Fact]
    public void IsTrue()
    {
        int value1 = 1;
        int value2 = 2; 
        Assert.True(value1 < value2, "2 should be higher than 1");
    }

}
